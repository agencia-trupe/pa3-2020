<?php
/**
 * Plugin para envio de Email
 */
class Mail extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request) 
	{
		//
    }
	
    public function getConfigs()
    {
        $configs = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini',APPLICATION_ENV);
        return $configs->mailer;
    }
    
	public function send($email,$nome,$sub,$html,$text=null){
        $configs = self::getConfigs();
        
        // $smtp = APPLICATION_ENV=='production' ? null :
                // new Zend_Mail_Transport_Smtp($configs->smtp->server,$configs->smtp->toArray());
        $smtp = new Zend_Mail_Transport_Smtp($configs->smtp->server,$configs->smtp->toArray());
        
        $text = utf8_decode($text ? $text : strip_tags(Php_Html::toText($html)));
        $html = "<html><head></head><body style='font-size:12px'>".
                "<div style='border-bottom:1px solid #ccc;padding-bottom:10px;'>".
                "<img src='cid:thelogo'/></div>".
                utf8_decode($html).
                mail_rodape().
                "</body></html>";
        $subject = utf8_decode($sub);
        
        $mail = new Zend_Mail();
        $mail->setBodyText($text);
        $mail->setBodyHtml($html);
        $mail->setFrom($configs->params->from,utf8_decode($configs->params->name));
        $mail->addTo($email,$nome);
        $mail->setSubject($subject);

        //$mail->addHeader('Return-Receipt-To',$configs->params->from); // confirmação
        if(APPLICATION_ENV!='development'){
            $mail->setReturnPath($configs->params->from);
            // $mail->addHeader("MIME-Version", "1.1");
            // $mail->addHeader("Content-type", "text/plain; charset=iso-8859-1");
            // $mail->addHeader("From", $configs->params->from);
            // $mail->addHeader("Return-Path", $configs->params->from);
        }

        // $mail->addBcc($configs->params->bcc,$configs->params->bcc);

        self::addLogo($mail); // logo
        $mail->send($smtp);
    }
    
    public function sendFollowUp($sub,$html)
    {
        $configs = self::getConfigs();
        self::send($configs->params->to,$configs->params->name,$sub,$html);
    }
    
    public function sendWithReply($email,$nome,$sub,$html,$text=null,$attachs=null,$params=array()){
		$configs = self::getConfigs();
        // $smtp = APPLICATION_ENV=='production' ? null :
                // new Zend_Mail_Transport_Smtp($configs->smtp->server,$configs->smtp->toArray());
        $smtp = new Zend_Mail_Transport_Smtp($configs->smtp->server,$configs->smtp->toArray());
        
        $text = utf8_decode($text ? $text : strip_tags(Php_Html::toText($html)));
        $html = "<html><head></head><body style='font-size:12px'>".
                "<div style='border-bottom:1px solid #ccc;padding-bottom:10px;'>".
                "<img src='cid:thelogo'/></div>".
                utf8_decode($html).
                mail_rodape().
                "</body></html>";
        $subject = utf8_decode($sub);
        
        $mail = new Zend_Mail();
        $mail->setBodyText($text);
        $mail->setBodyHtml($html);
        $mail->setFrom($configs->params->from,utf8_decode($configs->params->name));
        
        if(array_key_exists('to', $params)) foreach(explode(';',$params['to']) as $to) $mail->addTo(trim($to),trim($to));
            else $mail->addTo($configs->params->to,utf8_decode($configs->params->name));
        if(array_key_exists('bcc', $params)) $mail->addBcc($params['bcc'],$params['bcc']);
        // $mail->addBcc($configs->params->bcc,$configs->params->bcc);
		
        $mail->setReplyTo($email,utf8_decode($nome));
        $mail->setSubject($subject);
        //$mail->addHeader('Return-Receipt-To',$configs->params->from); // confirmação
        
        self::addLogo($mail); // logo
        
        if($attachs !== null){
            foreach($attachs as $at){
                $a = new Zend_Mime_Part(file_get_contents($at['path']));
                $a->type = $at['type'];
                $a->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $a->encoding = Zend_Mime::ENCODING_BASE64;
                $a->filename = $at['namesubs'] !== null ?
                               $at['namesubs'].'.'.end(explode('.',$at['path'])) :
                               end(explode('/',$at['path']));
                $mail->addAttachment($a);
            }
        }
        
        $mail->send($smtp);
    }
    
    public function addLogo(&$mail)
    {
        $mail->setType(Zend_Mime::MULTIPART_RELATED);
        $logo = $mail->createAttachment(file_get_contents(getcwd().'/public/css/img/logotipo.png'));
        $logo->type = 'image/png';
        $logo->disposition = Zend_Mime::DISPOSITION_INLINE;
        $logo->encoding = Zend_Mime::ENCODING_BASE64;
        $logo->filename = "logo.png";
        $logo->id = "thelogo";
    }
    
    public function sendError($sub,$html,$text=null){
        $configs = self::getConfigs();
        self::send($configs->params->error,$configs->params->error,$sub,$html,$text);
    }
}
