/* admin/search.js */
$(document).ready(function(){$(".search-result table tbody tr").mouseenter(function(){$(this).addClass("over");}).mouseleave(function(){$(this).removeClass("over");});$(".search-result table tbody tr td a.del").click(function(e){var elm=$(this);Msg.reset();if(confirm("Deseja realmente excluir o registro?")){e.preventDefault();var url=$(this).attr("href").replace("del","del.json");$.getJSON(url,function(data){if(data.erro){Msg.add(data.erro,"erro").show();}else{Msg.add("Registro excluído com sucesso.","message").show();elm.parents("tr").fadeOut("slow",function(){$(this).remove();});}});}
return false;});});

