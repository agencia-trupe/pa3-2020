// head.js features
head.feature('placeholder', function() {
    var t = document.createElement('textarea'); // #=> <textarea></textarea>
    return (t.placeholder !== undefined); // #=> true || false
});

head.feature('number', function() {
    var t = document.createElement('input'); 
    // if(!$.browser.msie) t.type = 'number'; // #=> <input type="number">
    return (t.max !== undefined); // #=> true || false
});

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

// checando features
if(!head.placeholder){
    var $elm;

    $('[placeholder]').each(function(){
        $elm = $(this);

        $elm.addClass('prevalue')
            .data('prevalue',$elm.attr('placeholder'))
            .prevalue(); 
    });
}

if(!head.number){
    var $elm;

    $('input[type=number]').each(function(){
        $elm = $(this);

        $elm.addClass('prevalue').addClass('mask-int')
            .data('prevalue',$elm.attr('placeholder'))
            .prevalue();

        _onlyNumbers('.mask-int');
    });
}

$('.input-number').on('blur',function(){
    var $this = $(this),
        val = Number($this.val());

    if($this.attr('max')) {
        var max = Number($this.attr('max'));
        
        if(max < val) {
            alert2('O valor está acima do permitido ('+max+')');
            window.setTimeout(function(){$this.focus();},100);
        }
    }

    if($this.attr('min')) {
        var min = Number($this.attr('min'));

        if(min > val) {
            alert2('O valor está abaixo do permitido ('+min+')');
            window.setTimeout(function(){$this.focus();},100);
        }
    }
});