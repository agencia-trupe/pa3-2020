<?php
$_ts = microtime(); $_ts = explode(' ', $_ts); $_ts = $_ts[1] + $_ts[0];

define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
define('APP_PATH', APPLICATION_PATH);

define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
define('APP_ENV', APPLICATION_ENV);
define('ENV_DEV', APP_ENV=='development');
define('ENV_PRO', APP_ENV=='production');
define('ENV_TEST',APP_ENV=='testing'||APP_ENV=='staging');
define('DS', DIRECTORY_SEPARATOR);
// if(ENV_DEV) error_reporting(E_ALL); ini_set('display_errors', 'On');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../../library'),
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/models'),
    get_include_path(),
)));

function __autoload($class){
    require_once(implode("/",explode("_",$class)).".php");
}

// Requires
// Lib::import('*'); Lib::import('helper.*');
function req($fs) { is_array($fs) ? reqa($fs) : req1($fs); } function req1($fs){ require_once($fs.'.php'); } function reqa($fs){ array_map(function($f){ require_once($f.'.php'); },$fs); }
reqa(array('Func','Js','Css','router','defines')); //_d(get_include_path());

/** Zend_Application */
//require_once 'Zend/Application.php';
// Create application, bootstrap, and run
$application = new Zend_Application(APP_ENV,APP_PATH.'/configs/application.ini');
$application->bootstrap()->run();
            
// if(ENV_DEV) $_tt = _tt($_ts,_tf(),0,0,0); echo $_tt.'s';
