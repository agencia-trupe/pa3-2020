<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
        $this->request =  new Zend_Controller_Request_Http();
        $this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');

        // db
        if(Zend_Registry::isRegistered('db')) $dbAdapter = Zend_Registry::get('db'); else {
            $config = new Zend_Config_Ini(APP_PATH.'/configs/application.ini',APP_ENV);
            // setup database
            $dbAdapter = Zend_Db::factory(
                $config->resources->db->adapter,
                $config->resources->db->params->toArray()
            );
            Zend_Db_Table::setDefaultAdapter($dbAdapter);
            Zend_Registry::set('db',$dbAdapter);
        }

        // cache tables metadata, db
        if(Zend_Registry::isRegistered('cache')) $cache = Zend_Registry::get('cache'); else {
            $cache = Zend_Cache::factory(
                'Core', 
                'File',
                array( // frontendOptions
                    'automatic_serialization' => true,
                    'lifetime' => 100000,
                    'cache_id_prefix' => 'metaDataFront_',
                ),
                array( // backendOptions
                    'automatic_serialization' => true,
                    'lifetime' => 100000,
                    'cache_id_prefix' => 'metaDataBack_',
                    'cache_dir' => CACHE_PATH,
                )
            );
            Zend_Registry::set('cache',$cache);
        }
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);

        
        // $this->is_module_admin = $this->request->getControllerName()=="admin";
        // $this->is_module_default = $this->request->getModuleName()=="default";
        $this->is_module_admin = (bool)strstr(FULL_URL, '/admin');
        $this->is_module_portal = (bool)strstr(FULL_URL, '/portal');
        $this->is_module_default = !$this->is_module_admin && !$this->is_module_portal;
        if(!$this->is_module_admin){
            // dados da empresa
            // $dados_empresa = db('dados_empresa');
            // $dados = Is_Array::utf8DbRow(
            //     $dados_empresa->fetchRow('id = 1')
            // );
            $dados = cache_get('dados_empresa',1);
            $this->view->dados = $dados; //_d($dados);

            // $users = cache_gets('usuarios','status_id=1'); _d($users);
            // $pag = cache_get('paginas',2); _d($pag);
            // $pags = cache_gets('paginas'); _d($pags);
            // $pags = cache_get_all('paginas','getPagina',2); _d($pags);
            
        }
        
        if($this->is_module_portal){
            $this->view->addScriptPath(APP_PATH.'/views/scripts');
            $this->view->addhelperPath(APP_PATH.'/views/helpers');
        }

        if($this->is_module_admin){
            $this->layout->setLayout("admin");
            
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            // $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            // $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
            
            $this->login = $auth->getIdentity();
            $this->view->isLogged = $this->view->logged = (bool)$this->login;
            $this->view->login = $this->login;
        }
        
        // if(array_key_exists('busca',$this->request->getParams())){
        //     $this->view->busca = $this->request->getParam('busca');
        // }

        $this->view->lang = 'pt';//$this->site_config->lang;
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
        
		switch($this->request->getControllerName()){
            case 'portal':
			case 'admin':
				$actionName = $this->request->getActionName();
				$actions = explode('/',$this->request->getRequestUri());
				$curAction = end($actions);
				$s = array_search($actionName,$actions);
				$action = $curAction!=$actions[$s]?$curAction:'index';
				//_d($action);
				$this->view->action = $action;
				$this->view->controller = $actionName;
				$this->view->module = $this->request->getControllerName();
				break;
			default:
				$this->view->action = $this->request->getActionName();
				$this->view->controller = $this->request->getControllerName();
				$this->view->module = $this->request->getModuleName();
		}
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        // if($this->request->getControllerName()=="admin"){   
        if($this->is_module_admin){
            // $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            // $config = $config->toArray();
            // $this->view->menu = new stdClass();

            // $menu_admin_top = $config['admin-top'];
            // self::configUrlPrefix($menu_admin_top);
            // $this->view->menu->admin_top  = new Zend_Navigation($menu_admin_top);
        } else {
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];

            // prefixos
            self::configUrlPrefix($menu_nav_top);
            // self::configUrlPrefix($menu_nav['footer']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav_top);
            // $this->view->menu->footer    = new Zend_Navigation($menu_nav['footer']);
			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}