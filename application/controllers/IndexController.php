<?php
req1('Mail');

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        // $this->banners = db('destaques');
    }

    public function indexAction()
    {
        // $banners = $this->banners->fetchAllWithPhoto('status_id=1','ordem');
        $banners = cache_get_all('destaques','bannersHome');
        $this->view->banners = $banners;
    }

    public function mailAction()
    {
        try{
            Mail::send('patrick.trupe@gmail.com','Trupe','Confirmação','Confirmar leitura do texto');
            echo "ok";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }

    public function imgAction()
    {
        $img_path = FULL_IMG_PATH.'/obras';
        // $img_path = FULL_IMG_PATH.'/clippings';
        // $img_path = FULL_IMG_PATH.'/paginas';
        // $img_path = FULL_IMG_PATH.'/destaques';
        $cnt = 0;

        foreach(glob($img_path.'/*.*') as $img){
            $i = end(explode('/', $img));
            $sz = filesize($img);
            
            if($cnt > 30) break;
            if(strstr($i, '_')) continue;
            // if($sz<5000) continue;
            if($sz<200000) continue;
            // if($i!='158955863215895586328364227788.png') continue;
            
            echo $img.' - '.$sz.' - '.Is_File::formatBytes($sz).'<br>';

            $q = 80;
            if($sz>300000) $q = 70;
            if($sz>400000) $q = 50;
            if($sz>500000) $q = 40;
            if($sz>600000) $q = 30;
            if($sz>700000) $q = 20;

            $img_opt = array(
                'resize'    => array(1000,1000),
                'dpi'       => array(72,72),
                'quality'   => $q,
            );
            Image::process($img,$img_opt);

            $cnt++;
        }

        // _d($img_path);
        _d('ok');
    }


}