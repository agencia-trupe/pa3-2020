<?php

class ArquitetosController extends ZendPlugin_Controller_Action
{

    public function init()
    {
        // $this->paginas = db_table('paginas');
        // $this->arquitetos = db_table('arquitetos');
    }

    public function indexAction()
    {
        // $this->view->pagina = $this->paginas->getPagina(4);
        $this->view->pagina = cache_get_all('paginas','getPagina',4);

        // $rows = $this->arquitetos->getAll(); //_d($rows);
        $rows = cache_get_all('arquitetos','getAll'); //_d($rows);

        $this->view->rows = $rows;
        $this->view->arquitetos = $rows;
        // _d($this->view->rows);

        return $rows;
    }

}

