<?php

class Application_Model_Db_ArquitetosFotos extends Zend_Db_Table
{
    protected $_name = "arquitetos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Arquitetos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Arquitetos' => array(
            'columns' => 'arquiteto_id',
            'refTableClass' => 'Application_Model_Db_Arquitetos',
            'refColumns'    => 'id'
        )
    );
}
