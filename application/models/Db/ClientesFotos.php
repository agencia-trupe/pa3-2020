<?php

class Application_Model_Db_ClientesFotos extends Zend_Db_Table
{
    protected $_name = "clientes_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Clientes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Clientes' => array(
            'columns' => 'cliente_id',
            'refTableClass' => 'Application_Model_Db_Clientes',
            'refColumns'    => 'id'
        )
    );
}
