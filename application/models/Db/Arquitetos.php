<?php

class Application_Model_Db_Arquitetos extends ZendPlugin_Db_Table 
{
    protected $_name = "arquitetos";
    protected $_foto_join_table = 'arquitetos_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'arquiteto_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    

    public function getAll()
    {
        return $this->fetchAllWithPhoto('status_id=1','t1.ordem');
    }
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da arquiteto
     *
     * @param int $id - id da arquiteto
     *
     * @return array - rowset com fotos da arquiteto
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('arquitetos_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('arquiteto_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos arquitetos
     * 
     * @param array $arquitetos - rowset de arquitetos para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - arquitetos com fotos
     */
    public function getFotos($arquitetos,$withObjects=false)
    {
        $pids = array(); // ids de arquitetos

        // identificando arquitetos
        foreach($arquitetos as $arquiteto) $pids[] = $arquiteto->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('arquitetos_fotos as fc',array('fc.arquiteto_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.arquiteto_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eArquitetos_fotos = new Application_Model_Db_ArquitetosFotos();
            $fotos = $eArquitetos_fotos->fetchAll('arquiteto_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($arquitetos as &$arquiteto){
            $arquiteto->fotos = $this->getFotosSearch($arquiteto->id,$fotos);
        }

        return $arquitetos;
    }

    /**
     * Monta rowset de fotos com base no arquiteto_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->arquiteto_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}