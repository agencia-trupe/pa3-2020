<?php

class Application_Model_LoginCliente
{
    public function checkAuth(&$c,$redirect=false){
        $login = new Zend_Session_Namespace(SITE_NAME."_login_cliente");
        
        if (!Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME.'_cliente'))->hasIdentity() ||
            !isset($login->user) ||
            !isset($login->acl) ||
            @$login->acl != SITE_NAME) {
            return self::logout($c,true);
            // return is_array($redirect) ? self::redirect($c,false,$redirect) : self::logout($c);
            //return self::redirect($c);
        } else {
            $acl = new Zend_Acl();
            $acl->addRole(new Zend_Acl_Role(SITE_NAME))->add(new Zend_Acl_Resource(SITE_NAME))->allow(SITE_NAME,SITE_NAME);
            
            if(!$acl->isAllowed($login->acl,SITE_NAME)){
                return is_array($redirect) ? self::redirect($c,false,$redirect) : self::logout($c);
            }
        }
    }
    
    public function isLogged()
    {
        $login = new Zend_Session_Namespace(SITE_NAME."_login_cliente");
        //return isset($login->user);
        return Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME.'_cliente'))->hasIdentity();
    }
    
    /**
     * Processa login
     */
    public function process($values)
    {
		$this->clientes = new Application_Model_Db_Clientes();
        if(!strstr($values['login'], '@')) $values['login'] = Is_Cpf::clean($values['login']);
        $usuario = $this->clientes->findByLoginAtivo($values['login'],$values['senha']);
        return $usuario ? self::setLogin($values) : false;
	}
    
    public function setLogin($values)
    {
        $adapter = self::_getAuthAdapter($values['login']);
        $adapter->setIdentity($values['login']);
        $adapter->setCredential($values['senha']);
        
        $auth = Zend_Auth::getInstance();
        $auth->setStorage(new Zend_Auth_Storage_Session(SITE_NAME.'_cliente'));
        $result = $auth->authenticate($adapter);
        
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();
            $auth->getStorage()->write($user);
            
            $sess_login = new Zend_Session_Namespace(SITE_NAME."_login_cliente");
            $sess_login->acl = SITE_NAME;
            $sess_login->user = $user;
            $sess_login->has_treinamentos = (int)$user->cliente_treinamentos;
            $sess_login->has_consultoria = (int)$user->cliente_consultoria ? (int)$user->cliente_consultoria : (int)$user->consultoria_id;
            $sess_login->consultoria = null;
            if($sess_login->has_consultoria){
                $clientes = new Application_Model_Db_Clientes();
                $sess_login->consultoria = $clientes->fetchRow('id="'.$user->consultoria_id.'"');
                $sess_login->instrucoes_gerais = $clientes->getConsultInstrucoes($user->consultoria_id);
            }
            
            return true;
        }
        return false;
    }
    
    public function logout(&$c,$logout=false)
    {
        Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME.'_cliente'))->clearIdentity();
		Zend_Session::destroy();
		return self::redirect($c,$logout);
    }
    
    public function redirect(&$c,$logout=false,$redirector=false)
    {
        $moduleName = $c->getRequest()->getModuleName();
		$controller = $c->getRequest()->getControllerName();
		$action     = $c->getRequest()->getActionName();
        
        return $c->getHelper("redirector")->goToRoute($redirector ? $redirector : array(
            'controller' => $logout?'login':'index',
            // 'controller' => $logout?'login':'treinamentos',
            // 'controller' => $logout?'treinamentos':'login',
            // 'controller' => 'treinamentos',
            // 'module' => 'default',
            'module' => 'portal',
            // 'module' => $moduleName,
            'action' => 'index'
        ), null, true);
    }
    
    /**
     * Adaptador do Objeto Zend_Auth
     */
    public function _getAuthAdapter($indentity){
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
		
		$authAdapter->setTableName('clientes')
            ->setIdentityColumn(stripos($indentity, '@') ? 'email' : 'cpf')
			->setCredentialColumn('senha')
			->setCredentialTreatment('MD5(?)');
			
		return $authAdapter;
	}
}
